<?php
  require_once "./code.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S01: PHP Basics and Selection Control</title>
</head>
<body>
  <!-- <h1>Hello World</h1> -->
  <h1>Echoing Values</h1>
  <p><?php echo 'Good day $name! Your given email is $email'; ?></p>
  <p><?php echo 'Good day ' . $name. '!' . ' Your given email is ' . $email . '.'; ?></p>
  <p><?php echo "Good day $name! Your given email is $email."; ?></p>
  <p><?php echo PI; ?></p>
  <p><?php echo $headCount; ?></p>

  <h2>Data Types</h2>

  <p>Strings</p>
  <p><?php echo $address; ?></p>

  <p>Integers/Float</p>
  <p><?php echo $age; ?></p>
  <p><?php echo $grade; ?></p>

  <p>Boolean/Null</p>
  <p><?php echo "hasTravelledAbroad $hasTravelledAbroad" ?></p>
  <p><?php echo "spouse $spouse" ?></p>

  <!-- gettype() or var_dump -->
  <!-- gettype() returns the type of a variable -->
  <p><?php gettype($hasTravelledAbroad); ?></p>
  <p><?php gettype($spouse); ?></p>

  <!-- var_dump function dumps information about one or more variables -->
  <p><?php var_dump($hasTravelledAbroad); ?></p>
  <p><?php var_dump($spouse); ?></p>

  <p>Arrays</p>
  <p><?php var_dump($grades) ?></p>
  <p><?php print_r($grades) ?></p>
  <p><?php echo $grades[3] ?></p>

  <p>Objects</p>
  <p> <?php var_dump($personObj); ?> </p>
	<p> <?php print_r($personObj); ?> </p>
	<p> <?php echo $gradesObj->firstGrading; ?> </p>
	<p> <?php echo $personObj->address->state; ?> </p>

  <h2>Operators</h2>
  <p>X: <?php $x; ?></p>
  <p>Y: <?php $y; ?></p>

  <p>Is Legal Age: <?php var_dump($isLegalAge); ?></p>
  <p>Is Registered: <?php var_dump($isRegistered); ?></p>

  <h2>Arithmetic Operators</h2>
  <p>Sum: <?php echo $x + $y; ?></p>
  <p>Difference: <?php echo $x - $y; ?></p>
  <p>Product: <?php echo $x * $y; ?></p>
  <p>Quotient: <?php echo $x / $y; ?></p>
  <p>Modulo: <?php echo $x % $y; ?></p>

  <h2>Equality Operators</h2>
  <p>Loose Equality:  <?php var_dump($x == '500') ?></p>  
  <p>Strict Equality:  <?php var_dump($x === '500') ?></p>  
  <p>Loose Inequality:  <?php var_dump($x != '500') ?></p>  
  <p>Strict Inequality:  <?php var_dump($x !== '500') ?></p>  

  <h2>Greater/Lesser Operators</h2>
  <p>Is Lesser: <?php var_dump($x < $y) ?></p>
  <p>Is Greater: <?php var_dump($x > $y) ?></p>

  <p>Is Lesser or Equal: <?php var_dump($x <= $y); ?></p>
  <p>Is Greater or Equal: <?php var_dump($x >= $y); ?></p>

  <h2>Logical Operators</h2>
  <p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
  <p>Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
  <p>Are Some Requirements are Not Met: <?php var_dump(!$isLegalAge || $isRegistered); ?></p>

  <h2>Functions</h2>
  <p>fullName: <?php echo getFullName("John", "D.", "Smith"); ?></p>

  <h2>Selection Control Structures</h2>
  <h3>If-Else-if-Else</h3>
  <p><?php echo determineTyphoonIntensity(23); ?></p>

  <h2>Conditional (Ternary) Operator</h2>
  <p>25: <?php var_dump(isUnderAge(25)); ?></p>
  <p>78: <?php var_dump(isUnderAge(78)); ?></p>

  <h2>Switch</h2>
  <p><?php echo determineComputerUser(4); ?></p>

  <h2>Try-Catch-Finally</h2>
  <p><?php greeting('Hello') ?></p>
  <p><?php greeting(25) ?></p>

</body>
</html>