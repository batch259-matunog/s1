<?php
  // Comments
  /*
    There are two types of comments
      Single-line comment - ctrl + /
      Multi-line comment - ctrl + shift +/
  */

  // Variables
  // Variables are defined using the dollar ($) notation before the name of the variable
  // (") or (')

  $name = 'John Smith';
  $email = 'johnsmith@gmail.com';

  // Constants
  // COnstanst used to hold data that are meant to be read only.
  // COnstants are defined using the define() function
  define('PI', 3.1416);

  // Echoing Values

  // Data Types
    // 1. Strings
      $state = 'New York';
      $country = 'United States of America';
      $address = "$state, $country";

    // 2. Integers/Whole Numbers
      $age = 18;
      $headCount = 16;

    // 3. Float/Decimal Numbers
      $grade = 98.2;
      $distanceInKilometers = 1245.89;

    // 4. Boolean
      $hasTravelledAbroad = false;
      $haveSymptoms = true;

    // Null
      $spouse = null;
      $middleName = null;

    // Arrays 
      $grades = array(98.7, 92.1, 90.2, 94.6);
      $animals = ["Dogs", "Cat", "Chicken"];

    // Objects
      $gradesObj = (object)[
        'firstGrading' => 98.7,
        'secondGrading' => 92.1,
        'thirdGrading' => 90.2,
        'fourthGrading' => 94.6
      ];

      $personObj = (object)[
        'fullName' => 'John Smith',
        'isMarried' => false,
        'age' => 18,
        'address' => (object)[
          'state' => 'New York',
          'country' => 'United States of America'
        ]
      ];

// Assignment Operators
  $x = 6;
  $y = 7;

  $isLegalAge = true;
  $isRegistered = false;

// Functions
  function getFullName($firstName, $middleName, $lastName) {
    return "$lastName, $firstName $middleName";
  }

// Selection Control Structures
  // If-Elseif-Else Statement
  function determineTyphoonIntensity ($windSpeed) {
    if($windSpeed < 30){
      return 'Not a Typhoon yet.';
    } else if ($windSpeed <= 61) {
      return 'Tropical Depression Detected';
    } else if ($windSpeed >= 62 && $windSpeed <= 88) {
      return 'Tropical Storm Detected';
    } else if ($windSpeed >= 89 && $windSpeed <=117) {
      return 'Severe tropical storm detected';
    } else {
      return 'typhoon detected';
    }
  }

// Conditional (Ternary) Operator
function isUnderAge($age) {
  return ($age < 18) ? true : false;
}

function determineComputerUser($computerNumber){
  switch($computerNumber) {
    case 1:
      return 'Linus Torvals';
      break;
    case 2:
      return 'Steve Jobs';
      break;
    case 3:
      return 'Sid Meier';
      break;
    case 4:
      return 'Onel de Guzman';
      break;
    case 5:
      return 'Cristian Salvador';
      break;
    default:
      return "$computerNumber is out of bounds";
      break;
  }
}

// Try-Catch-Finally Statement 
function greeting($str){
  try {
    if(gettype($str) === "string"){
      echo $str;
    } else {
      throw new Exception("Oops!");
    }
  } catch (Exception $e){
    echo $e -> getMessage();
  } finally {
    echo ", I did it again!";
  }
}

?>